#include <iostream>
#include <vector>
#include <string.h>
#include <queue>
#include <algorithm>
using namespace std;

class Tree{
	public:
		Tree(size_t n) : numberOfNodes(n) {
			connections = new vector<size_t>[n+1];
			visitedNodes = new bool[n+1]();
			levels = new size_t[n+1]();
			numberOfUnvisitedNeighbours = new size_t[n+1]();
			numberOfLevels = 0;
			whichLevel = new size_t[1+n]();

			for(size_t i = 0; i < n+1; ++i){
				visitedNodes[n] = false;
				levels[n] = 0;
				numberOfUnvisitedNeighbours[i] = 0;
				whichLevel[n] = 0;
			}
		}

		~Tree(){
			delete [] connections;
			delete []visitedNodes;
			delete []levels;
			delete []numberOfUnvisitedNeighbours;
			delete []whichLevel;
		}
		bool isVisited(size_t n) const {
			return visitedNodes[n];
		}

		void setVisited(size_t n){
			visitedNodes[n] = true;
		}

		void resetVisited(){
			memset(visitedNodes, false, numberOfNodes + 1);
		}

		void createLevels() {
			queue<size_t> toCount;
			for(size_t i = 1; i <= numberOfNodes; ++i){
				if(numberOfUnvisitedNeighbours[i] == 1){
				toCount.push(i);
				setVisited(i);
				//levlels[i] = 1;
				}
			}

			while(!toCount.empty()){
				size_t currentNode = toCount.front();
				toCount.pop();
				size_t maximumLevel = 0;
				for(size_t i = 0; i<connections[currentNode].size(); ++i){
					maximumLevel = max(maximumLevel, whichLevel[connections[currentNode][i]]);
					if(!isVisited(connections[currentNode][i]) && --numberOfUnvisitedNeighbours[connections[currentNode][i]] <= 1){
						toCount.push(connections[currentNode][i]);
						setVisited(connections[currentNode][i]);
					}
				}
				++maximumLevel;
				numberOfLevels = max(numberOfLevels, maximumLevel);
				++levels[maximumLevel];
				whichLevel[currentNode] = maximumLevel;
			}
			/*
			for(size_t i = 1; i <= numberOfLevels; ++i){
				cout<<i<<": "<<levels[i]<<endl;
			}
			*/
		}
		size_t getResult() const{
			size_t result = 0;
			for(size_t i =1; i <= numberOfLevels; ++i){
				result += min(2*l, levels[i]);
			}
			return result;
		}

		size_t printResult() const{
			cout<<getResult();
		}

		void loadParameters(){
			cin>>this->l;
			size_t xo, yo;
			for (size_t i = 1; i < numberOfNodes; ++i){
				cin>>xo>>yo;
				++numberOfUnvisitedNeighbours[xo];
				++numberOfUnvisitedNeighbours[yo];
				connections[xo].push_back(yo);
				connections[yo].push_back(xo);
			}
		}

	private:
		const size_t numberOfNodes;
		bool *visitedNodes;
		vector<size_t> *connections;
		size_t *levels;
		size_t *numberOfUnvisitedNeighbours;
		//vector<size_t>numberOfNodesInLevel;
		size_t numberOfLevels;
		size_t l;
		size_t *whichLevel;
};



int main(){
	ios_base::sync_with_stdio(0);
	size_t x;
	cin>>x;
	Tree a(x);
	a.loadParameters();
	a.createLevels();
	a.printResult();
    return 0;
}
